/* Copyright 1997 Sven B. Schreiber
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// CabLib.h
// 01-20-1997 Sven B. Schreiber

#ifdef __riscos
typedef char            BYTE;
typedef char          * PBYTE;

typedef int             DWORD;
typedef int           * PDWORD;

typedef short           WORD;
typedef short         * PWORD;

typedef int             HWND;

typedef int             BOOL;
#endif

// =================================================================
// CAB FILE LAYOUT
// =================================================================

/*

(1) CAB_HEADER structure
(2) Reserved area, if CAB_HEADER.flags & CAB_FLAG_RESERVE
(3) Previous cabinet name, if CAB_HEADER.flags & CAB_FLAG_HASPREV
(4) Previous disk name, if CAB_HEADER.flags & CAB_FLAG_HASPREV
(5) Next cabinet name, if CAB_HEADER.flags & CAB_FLAG_HASNEXT
(6) Next disk name, if CAB_HEADER.flags & CAB_FLAG_HASNEXT
(7) CAB_FOLDER structures (n = CAB_HEADER.cFolders)
(8) CAB_ENTRY structures / file names (n = CAB_HEADER.cFiles)
(9) File data (offset = CAB_FOLDER.coffCabStart)

*/

// =================================================================
// MACROS
// =================================================================

#define SWAPWORD(x)  ((WORD) (((x) << 8) | ((x) >> 8)))

#define SWAPDWORD(x) ((SWAPWORD ((WORD) (x)) << 16) | \
                      (SWAPWORD ((WORD) ((x) >> 16))))

// =================================================================
// CONSTANTS
// =================================================================
#define CAB_SIGNATURE        SWAPDWORD (('M'<<24)|('S'<<16)|('C'<<8)|('F'<<0))
#define CAB_VERSION          0x0103

#define CAB_FLAG_HASPREV     0x0001
#define CAB_FLAG_HASNEXT     0x0002
#define CAB_FLAG_RESERVE     0x0004

#define CAB_ATTRIB_READONLY  0x0001
#define CAB_ATTRIB_HIDDEN    0x0002
#define CAB_ATTRIB_SYSTEM    0x0004
#define CAB_ATTRIB_VOLUME    0x0008
#define CAB_ATTRIB_DIRECTORY 0x0010
#define CAB_ATTRIB_ARCHIVE   0x0020

#define CAB_FILE_FIRST       0x0000
#define CAB_FILE_NEXT        0x0001
#define CAB_FILE_SPLIT       0xFFFE
#define CAB_FILE_CONTINUED   0xFFFD

#define CAB_NOTIFY_OK        1
#define CAB_NOTIFY_ERROR     0
#define CAB_NOTIFY_SKIP      0
#define CAB_NOTIFY_ABORT     (-1)

#define MAX_MESSAGE          1024

// =================================================================
// CABINET STRUCTURES
// =================================================================

typedef struct _CAB_HEADER
    {
    DWORD sig;              // file signature 'MSCF' (CAB_SIGNATURE)
    DWORD csumHeader;       // header checksum (0 if not used)
    DWORD cbCabinet;        // cabinet file size
    DWORD csumFolders;      // folders checksum (0 if not used)
    DWORD coffFiles;        // offset of first CAB_ENTRY
    DWORD csumFiles;        // files checksum (0 if not used)
    WORD  version;          // cabinet version (CAB_VERSION)
    WORD  cFolders;         // number of folders
    WORD  cFiles;           // number of files
    WORD  flags;            // cabinet flags (CAB_FLAG_*)
    WORD  setID;            // cabinet set id
    WORD  iCabinet;         // zero-based cabinet number
    }
    CAB_HEADER, *PCAB_HEADER;
#define CAB_HEADER_ sizeof (CAB_HEADER)

// -----------------------------------------------------------------

typedef struct _CAB_FOLDER
    {
    DWORD coffCabStart;     // offset of folder data
    WORD  cCFData;          // ???
    WORD  typeCompress;     // compression type (tcomp* in FDI.h)
    }
    CAB_FOLDER, *PCAB_FOLDER;
#define CAB_FOLDER_ sizeof (CAB_FOLDER)

// -----------------------------------------------------------------

typedef struct _CAB_ENTRY
    {
    DWORD cbFile;           // uncompressed file size
    DWORD uoffFolderStart;  // file offset after decompression
    WORD  iFolder;          // file control id (CAB_FILE_*)
    WORD  date;             // file date stamp, as used by DOS
    WORD  time;             // file time stamp, as used by DOS
    WORD  attribs;          // file attributes (CAB_ATTRIB_*)
    }
    CAB_ENTRY, *PCAB_ENTRY;
#define CAB_ENTRY_ sizeof (CAB_ENTRY)

// =================================================================
// AUXILIARY STRUCTURES
// =================================================================

typedef struct _CAB_CONTROL
    {
    int   hf;
    int   pbPrevCab;
    int   pbPrevDisk;
    int   pbNextCab;
    int   pbNextDisk;
    int   pReserve;
    DWORD dReserve;
    DWORD dSetId;
    DWORD dCabNumber;
    DWORD dFolders;
    DWORD dFiles;
    DWORD dExtraData;
    DWORD dCabList;
    DWORD dFolderList;
    DWORD dFileList;
    DWORD dFileData;
    DWORD dCabSize;
    DWORD dOffset;
    DWORD dSize;
    }
    CAB_CONTROL, *PCAB_CONTROL, *HCABINET;
#define CAB_CONTROL_ sizeof (CAB_CONTROL)

// -----------------------------------------------------------------

typedef struct _EXTRACT_FILE
    {
    HWND  hWnd;
    PBYTE pbEntry;
    PBYTE pbFile;
    }
    EXTRACT_FILE, *PEXTRACT_FILE;
#define EXTRACT_FILE_ sizeof (EXTRACT_FILE)

// =================================================================
// END OF FILE
// =================================================================
