/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/Codecs/SparkCab/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.arcs */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "kernel.h"
#include "swis.h"

#include "SparkLib/zflex.h"

#include "Interface/SparkFS.h"

#include "SparkLib/sfs.h"
#include "SparkLib/sarcfs.h"
#include "SparkLib/err.h"

#include "buffer.h"
#include "convert.h"
#include "cat.h"
#include "arcs.h"
#include "unpack.h"
#include "pack.h"


/*****************************************************************************/


static _kernel_oserror * openarcdatafile(archive * arc,int fn)
{
 _kernel_oserror * err;

 err=openarc(arc,'r');

 return(err);
}


static _kernel_oserror * closearcdatafile(archive * arc)
{
 _kernel_oserror * err;

 err=closearc(arc);

 return(err);
}


/****************************************************************************/

/* sequence should be                            */
/* 1 ensure enough memory for decompression code */
/* 2 ensure enough memory for destination        */
/* 3 ensure enough memory for source             */

/* output can be one of two things    */
/* 1 output to a fixed memory address */
/* 2 output to a filestructure        */


static char bitsbyte;

#define XMEM  1
#define XFILE 2


static int  sourcelen;


_kernel_oserror * loadfile(linkblock * linkb)
{
 _kernel_oserror * err;
 int               sfh;
 int               dfh;
 char            * srcbuffer;
 char            * destbuffer;
 buffer            srcb;
 buffer            destb;
 int               dlen;
 int               slen;
 archive         * arc;
 int               fn;

 arc =linkb->openload.arc;
 fn  =linkb->openload.fn;
 slen=linkb->loadfile.slen;
 dlen=linkb->loadfile.dlen;
 dfh =linkb->loadfile.dfh;
 srcbuffer=linkb->loadfile.src;
 destbuffer=linkb->loadfile.dest;

 sfh=arc->fh;

 if(arc->hdr[fn].hdrver!=0)
 {
  initbuff(&srcb,linkb->loadfile.src,sourcelen,slen,sfh);
  srcb.flags=BCODE;
  err=initsrcbuffer(&srcb);

  if(!err)
  {
   initbuff(&destb,linkb->loadfile.dest,arc->hdr[fn].length,dlen,dfh);
   destb.datastart=arc->hdr[fn].fp;

   if(!err) err=unpack(arc,fn,&srcb,&destb);
   finitbuff(&destb);
  }
  finitbuff(&srcb);
 }
 else
 {
  if(!dfh)
  {
   err=readblock(&linkb->loadfile.dest,sfh,arc->hdr[fn].length,
                                                  COPYCRC|COPYDECODE);
  }
  else
  {
   err=copyfile(sfh,dfh,arc->hdr[fn].length,COPYCRC|COPYDECODE);
  }
 }

 return(err);
}





_kernel_oserror * openload(linkblock * linkb)
{
 _kernel_oserror * err;
 archive         * arc;
 int               fn;
 int               slen;

 arc=linkb->openload.arc;
 fn =linkb->openload.fn;


 if(arc->hdr[fn].hdrver>1) err=geterror(mb_sfserr_NonMethod);
 else
 {
  err=openarcdatafile(arc,fn);
  if(!err)
  {
   err=lineupdata(arc,fn,&slen);

   if(!err) err=openunpack(arc,fn);

   if(err) closearcdatafile(arc);

   linkb->openload.slen=sourcelen=slen;
  }

  linkb->openload.dlen=arc->hdr[fn].length;
 }

 return(err);
}







_kernel_oserror * closeload(linkblock * linkb)
{
 _kernel_oserror * err;
 archive         * arc;
 int               fn;

 arc=linkb->closeload.arc;
 fn =linkb->closeload.fn;

 closeunpack();

 err=closearcdatafile(arc);

 return(err);
}



/*****************************************************************************/
/* save chunk of memory, dest is file handle to write it to */


static _kernel_oserror * savearcfilesub(heads * hdr,int sfh,char * start)
{
 _kernel_oserror * err;
 int               dest;
 char            * srcbuff;
 char            * destbuff;
 buffer            srcb;
 buffer            destb;
 int               sbsize;
 int               dbsize;

 srcbuff=destbuff=NULL;

 setcode();


 if(hdr->hdrver==0)
 {
  err=open(arcpath,'w',&dest);
 /* writefilehdr(dest,hdr->hdrver,hdr->name); */

  if(start)
  {
   srcbuff=start;
   err=writeblock(&start,dest,hdr->length,COPYCRC|COPYCODE);
  }
  else
  {
   err=copyfile(sfh,dest,hdr->length,COPYCRC|COPYCODE);
  }

  hdr->crc=copycrc;
  hdr->size=copylen;

  if(!err) err=close(dest);
  else         close(dest);
 }
 else
 {
  err=buff_alloc((flex_ptr)&destbuff,DEFPACKSIZE,&dbsize);
  if(!err) err=openpack(cxmethod,cxbits);

  if(!err)
  {
   err=open(arcpath,'w',&dest);
   if(!err)
   {
    if(!err)
    {
     if(start)
     {
      srcbuff=start;
      initbuff(&srcb,srcbuff,hdr->length,hdr->length,0);
     }
     else
     {
      err=buff_alloc((flex_ptr)&srcbuff,DEFPACKSIZE,&sbsize);
      if(!err) initbuff(&srcb,srcbuff,hdr->length,sbsize,sfh);
     }

     if(!err)
     {
      srcb.flags=BCRC;
      initsrcbuffer(&srcb);

      initbuff(&destb,destbuff,-1,dbsize,dest);
      destb.flags=BCODE;

     /* writefilehdr(dest,hdr->hdrver,hdr->name); */

      err=pack(hdr,&srcb,&destb);

      hdr->crc=srcb.crc;

      if(!start) flex_free((flex_ptr)&srcbuff);
     }
    }
    if(!err) err=close(dest);
    else         close(dest);
   }

   closepack();
  }
  if(destbuff) {flex_free((flex_ptr)&destbuff);deb("free destbuff");}
 }

 closecode();

 return(err);
}


_kernel_oserror * savefile(linkblock * linkb)
{
 _kernel_oserror * err;
 archive         * arc;
 heads           * hdr;
 int               ins;
 int               ind;
 int               ow;
 int               sfh;
 char            * start;
 int               nx;

 arc=linkb->savefile.arc;
 hdr=linkb->savefile.hdr;
 ins=linkb->savefile.ins;
 ind=linkb->savefile.ind;
 ow =linkb->savefile.ow;
 sfh=linkb->savefile.sfh;
 start=linkb->savefile.src;


 if(ow) nx=arc->hdr[ins].fp;
/* else   nx=nextfilenumber(arc); */
/* err=checkpath(arc->name,nx,!ow); */
 if(!err)
 {                                      /* first try to compress the file */
  hdr->hdrver=cxmethod|128;

  err=savearcfilesub(hdr,sfh,start);

  if(!err)
  {                                    /* now see if compression suceeded */
   if(hdr->size>hdr->length && ((cxmethod & 0x7F)!=0)) /* failed     */
   {                                   /* so have a go at just storing    */
    hdr->hdrver=0|128;
    err=savearcfilesub(hdr,sfh,start);
   }
  }

  if(!ow) err=insentry(arc,ins,ind,1);
  if(!err)
  {
   hdr->fp=nx;
   arc->hdr[ins]=*hdr;
/*   err=writesp2cat(arc->name,arc,ow?ins:-1); */
  }
 }
 return(err);
}



_kernel_oserror * opensave(linkblock * linkb)
{
 _kernel_oserror * err;
 linkb=linkb;

 err=NULL;

 return(err);
}


_kernel_oserror * closesave(linkblock * linkb)
{
 _kernel_oserror * err;
 linkb=linkb;

 err=NULL;

 return(err);
}
